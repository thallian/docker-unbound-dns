[Unbound](https://unbound.net/) as a validating, caching and recursing dns resolver.
Includes ad blocking via host files.

# Ports
- 53

# Volumes
- `/etc/unbound/custom`: Files in here get included into the the unbound configuration. Use it for stuff like blacklisting.

# Environment Variables
## HOST_BLOCK_FILE
- default: "http://sbc.io/hosts/alternates/fakenews-gambling/hosts"

URL to the blocker host file.

## NUM_THREADS
- default: 1

Number of threads to create. 1 disables threading.

## RATELIMIT
- default: 0

Ratelimit for uncached, new queries, this limits recursion effort.

## IP_RATELIMIT
- default: 0

Global query ratelimit for all ip addresses.

## ACCESS_CONTROL
- default: 127.0.0.1/0 allow

Semicolon seperated list of access-control directives.
