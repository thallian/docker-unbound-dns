FROM registry.gitlab.com/thallian/docker-confd-env:master

RUN apk add --no-cache unbound curl libressl

ADD /rootfs /

EXPOSE 53

VOLUME [ "/etc/unbound/custom" ]
